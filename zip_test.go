package corarchive

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

func TestZipUnZip(t *testing.T) {
	f, err := ioutil.TempFile("", "zzip")
	if err != nil {
		t.Fatal(err)
	}

	tmplFileName := f.Name()
	defer f.Close()
	defer os.Remove(tmplFileName)

	// Tar and close
	if err := Zip("test_files/zip_src/", tmplFileName); err != nil {
		t.Fatal(err)
	}
	if err := f.Close(); err != nil {
		t.Fatal(err)
	}

	// create a tmp directory
	outname, err := ioutil.TempDir("", "zunzip")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(outname)

	if err := Unzip(tmplFileName, outname); err != nil {
		t.Fatal(err)
	}

	checkFiles := []string{
		outname + `/zip_src/.hiddena`,
		outname + `/zip_src/filea`,
		outname + `/zip_src/inner/.hiddenb`,
		outname + `/zip_src/inner/fileb`,
	}

	for _, checkFile := range checkFiles {
		if err := testCheckFileExists(checkFile); err != nil {
			t.Fatal(err)
		}
	}
}

func testCheckFileExists(filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	if f == nil {
		return fmt.Errorf("%s was not found", filename)
	}
	return nil
}
